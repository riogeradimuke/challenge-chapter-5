const {
  cars
} = require("../models")

class carsRepository {
  static async getCars() {
      const getCars = cars.findAll();

      return getCars;
  }

  static async createCars({
    nama,
    harga,
    ukuran,
    foto
  }) {
      const createCars = cars.create({
        nama,
        harga,
        ukuran,
        foto
      })
      return createCars;
  }

  static async getById({
      id
  }) {
      const getByCarsId = await cars.findOne({
          where: {
              id
          }
      });
      return getByCarsId;
  }

  static async updateCarsById({
      id,
      nama,
      harga,
      ukuran,
      foto
  }) {
      const updateCarsById = cars.update({
          nama,
          harga,
          ukuran,
          foto
      }, {
          where: {
              id
          }
      })
      return updateCarsById;
  }

  static async deleteCarsById({
      id,
  }) {
      // let deletedUsers = {};
      const deleteCarsById = cars.destroy({
          where: { id }
      })
      // users = deletedUsers;
      return deleteCarsById;
  }

}

module.exports = carsRepository;