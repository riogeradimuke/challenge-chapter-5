const carsService = require("../services/carsService");

const getCars = async (req, res) => {
    const getCars = await carsService.getCars();

    // respon API Microservices
    // res.send(getCars);

    // respon data monolithic (EJS)
    return getCars;
};

const createCars = async (req, res) => {
    const {
        nama,
        harga,
        ukuran,
        foto
    } = req.body;

    const createCars = await carsService.createCars({
        nama,
        harga,
        ukuran,
        foto
    });

    res.redirect("/");
};

const updateCarsById = async (req, res) => {
    const {
        nama,
        harga,
        ukuran,
        foto
    } = req.body;
    const {
        id
    } = req.params;
    const updateCarsById = await carsService.updateCarsById({
        id,
        nama,
        harga,
        ukuran,
        foto
    });

    res.redirect("/");
}

const renderCarById = async (req, res) =>{
    const { id } = req.params;
    const car = await carsService.getById({ id });
    res.render('edit', {
        car: car,
    })
}

const getCarsAll = async (req, res) =>{
    const getCars = await carsService.getCars();
    res.render('index', {
        cars: getCars,
    })
}

const deleteCarsById = async (req, res) =>{
    const { id } = req.params;
    const deleteCarsById = await carsService.deleteCarsById({ id });
    
    res.redirect("/")
}

module.exports = {
    getCars,
    createCars,
    updateCarsById,
    renderCarById,
    getCarsAll,
    deleteCarsById
};