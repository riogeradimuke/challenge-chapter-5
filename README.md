Rio Geradi Muke

# Challenge-Binar-Chapter-5

link ERD
https://dbdiagram.io/d/626374fe1072ae0b6ad47308

tabel database CARS
Table cars {
id bigint [pk, increment] // auto-increment
nama varchar
harga integer
ukuran varchar
foto text
created_at timestamp
update_at timestamp
}

// endpoint crud
app.post("/cars", carsController.createCars);
app.post("/cars/:id", carsController.updateCarsById);
app.post("/deleteCars/:id", carsController.deleteCarsById);

// define endpoint ejs
app.get("/", carsController.getCarsAll)

app.get("/cars", (req, res) =>{
res.render('create')
})

app.get("/update/:id", carsController.renderCarById);
